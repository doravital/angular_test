import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import {FormGroup , FormControl,FormBuilder} from '@angular/forms';
import { ActivatedRoute,ParamMap,Router } from '@angular/router';
import { UsersService } from "../users.service";

@Component({
  selector: 'update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
  @Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();

  service:UsersService;

  updateform = new FormGroup({
    username:new FormControl(),
    email:new FormControl()
  });
username;
email;
id;
  
  constructor(service:UsersService, private formBuilder:FormBuilder, private route: ActivatedRoute, private router: Router) {   	    
    this.service = service;    
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = +params.get('id'); // This line converts id from string into num      
      this.service.getUser(this.id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user.name);
        this.username = this.user.username;
        this.email = this.user.email                                
      });      
    });
  }

  //שליחת העדכון
  sendData() {
    this.updateUser.emit(this.updateform.value.username);
    console.log(this.updateform.value);

    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putUser(this.updateform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.updateUserPs.emit();
          this.router.navigateByUrl("/");//return to main page
        }
      );
    })
  }

  //הצגת מספר מזהה בשביל הטופס
  user;
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
      })
    })
  }

}