import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
 users;
  usersKeys =[];

  //דרך אופטימית להוספת משתמש
  optimisticAdd(user){
    var newKey = parseInt(this.usersKeys[this.usersKeys.length - 1],0) + 1;
    var newUserObject = {};
    newUserObject['username'] = user;
    this.users[newKey] = newUserObject;
    this.usersKeys = Object.keys(this.users);
  }

  pessimiaticAdd(){
    this.service.getUsers().subscribe(response => {
      this.users =  response.json();
      this.usersKeys = Object.keys(this.users);
    });   
  }

  //מחיקת משתמש
  deleteUser(key){ //מימוש אופטימיסטיק דליט
     console.log(key);
     let index = this.usersKeys.indexOf(key); //מציאת המקום של הקי שנשלח בפונקציה
     this.usersKeys.splice(index,1); // מחיקת הערך מהמערך
 
   //delete from server
     this.service.deleteUser(key).subscribe(
       response=>console.log(response) // לצורך בדיקה
     );
   }
  constructor(private service:UsersService) {
    service.getUsers().subscribe(response=>{
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
    });
   }

  ngOnInit() {
  }

}


 
  

 
  