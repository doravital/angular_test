import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { UsersService } from './../users.service';




@Component({
  selector: 'userForm',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Output()
  addUser: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  addUserPs: EventEmitter<any> = new EventEmitter<any>();
  
  service:UsersService;
 userform = new FormGroup({
  username: new FormControl(),
   email: new FormControl()
  });

  sendData(){
    this.addUser.emit(this.userform.value.username);
    this.service.postUsers(this.userform.value).subscribe(response =>{
      this.addUserPs.emit();  
      console.log(response);
    });
  }


  constructor(service:UsersService, private formBuilder:FormBuilder) { 
    this.service = service;
  }

  ngOnInit() {
    //הגדרת ולידציה לטופס
    this.userform = this.formBuilder.group({
     username:  [null, [Validators.required]],
     email: [null, [Validators.required]],
   });
 }

}