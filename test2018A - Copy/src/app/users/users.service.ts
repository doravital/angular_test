import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { HttpParams } from '@angular/common/http';
//Q5
import { AngularFireDatabase } from 'angularfire2/database';

//Deployment
import { environment } from './../../environments/environment';


@Injectable()
export class UsersService {
  http:Http;

 //Q5
   getUsersFire(){
    return this.db.list('/users').valueChanges();
  }

getUsers(){
return this.http.get(environment.url +'users');
}

//Post
postUsers(data){
  let options = {
    headers: new Headers({
      'content-type':'application/x-www-form-urlencoded'
    })
  }
  //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
  let params = new HttpParams().append('username',data.username).append('email',data.email);
  return this.http.post(environment.url +'users', params.toString(), options);
}

//Delete
deleteUser(key){
  return this.http.delete(environment.url +'users/'+ key);
}

 //Get one user
 getUser(id){
  return this.http.get(environment.url +'users/'+ id);
}

//Put
putUser(data,key){
 let options = {
   headers: new Headers({
     'content-type':'application/x-www-form-urlencoded'
   })
 }
 //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
 let params = new HttpParams().append('username',data.username).append('email',data.email);
 return this.http.put(environment.url +'users/'+ key,params.toString(), options);
}
  constructor(http:Http, private db:AngularFireDatabase) { 
    this.http = http;
  }

}


