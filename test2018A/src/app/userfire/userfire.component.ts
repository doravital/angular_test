import { Component, OnInit } from '@angular/core';
import { UsersService } from './../users/users.service';

@Component({
  selector: 'userfire',
  templateUrl: './userfire.component.html',
  styleUrls: ['./userfire.component.css']
})
export class UserfireComponent implements OnInit {

   users;

  constructor(private service:UsersService) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(response=>{
        console.log(response);
        this.users = response;
    });
  }

}
