import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { HttpParams } from '@angular/common/http';
//Q5
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class UsersService {
  http:Http;

 //Q5
   getUsersFire(){
    return this.db.list('/users').valueChanges();
  }

getUsers(){
return this.http.get('http://localhost/angular/Slim/users');

}

//Post
postUsers(data){
  let options = {
    headers: new Headers({
      'content-type':'application/x-www-form-urlencoded'
    })
  }
  //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
  let params = new HttpParams().append('username',data.username).append('email',data.email);
  return this.http.post('http://localhost/angular/Slim/users', params.toString(), options);
}

//Delete
deleteUser(key){
  return this.http.delete('http://localhost/angular/Slim/users/'+ key);
}

 //Get one user
 getUser(id){
  return this.http.get('http://localhost/angular/Slim/users/'+ id);
}

//Put
putUser(data,key){
 let options = {
   headers: new Headers({
     'content-type':'application/x-www-form-urlencoded'
   })
 }
 //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
 let params = new HttpParams().append('username',data.username).append('email',data.email);
 return this.http.put('http://localhost/angular/Slim/users/'+ key,params.toString(), options);
}
  constructor(http:Http, private db:AngularFireDatabase) { 
    this.http = http;
  }

}


