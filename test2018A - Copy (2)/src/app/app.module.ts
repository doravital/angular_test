import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';


import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HttpModule } from '@angular/http';
import { UserFormComponent } from './users/user-form/user-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './users/user/user.component';
import { UsersService } from "./users/users.service";
import { UpdateFormComponent } from "./users/update-form/update-form.component";

//fire base
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';
import { UserfireComponent } from './userfire/userfire.component';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    UserFormComponent,
    UserComponent,
    UpdateFormComponent,
    UserfireComponent
  ],
  imports: [
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule.forRoot([
      {path:'', component:UsersComponent}, //default - localhost:4200 - homepage
      {path:'products', component:ProductsComponent},//localhost:4200/products
      {path: 'user/:id', component: UserComponent},
      {path: 'update-form/:id', component: UpdateFormComponent},
      {path: 'userfire', component: UserfireComponent},
      {path:'**', component:NotFoundComponent} //all the routs that don't exist
    ])
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }