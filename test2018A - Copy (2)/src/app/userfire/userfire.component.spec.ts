import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserfireComponent } from './userfire.component';

describe('UserfireComponent', () => {
  let component: UserfireComponent;
  let fixture: ComponentFixture<UserfireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserfireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserfireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
